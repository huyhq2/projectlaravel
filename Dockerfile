FROM php:8-fpm-alpine

ENV LANG C.UTF-8

RUN echo http://dl-2.alpinelinux.org/alpine/edge/community/ >> /etc/apk/repositories
WORKDIR /app